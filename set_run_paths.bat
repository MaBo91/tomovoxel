
if NOT DEFINED QTDIR (
	set QTDIR=C:\Qt\5.12.9\mingw73_32
)
if NOT DEFINED MINGWDIR (
	set MINGWDIR=C:\Qt\Tools\mingw730_32
)

set PATH=%QTDIR%\bin;%MINGWDIR%\bin;%PATH%
