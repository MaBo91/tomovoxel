#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "VoxelVolume.h"
#include "VolumeGenerator.h"

#include "TomoVoxel_c.h"

using namespace voxel;

TEST(TomoVoxel, hello42)
{

}

TEST(TomoVoxel, setAndGetVoxel)
{
    VoxelVolume voxelVolume = createSphericalDemoVolume();

    voxelVolume.setVoxel(40, 35, 30, 125);
    EXPECT_EQ(voxelVolume.voxel(40, 35, 30), 125);

    voxelVolume.setVoxel(40, 35, 30, 126);
    EXPECT_EQ(voxelVolume.voxel(40, 35, 30), 126);

    voxelVolume.setVoxel(53, 16, 42, 55);
    EXPECT_EQ(voxelVolume.voxel(53, 16, 42), 55);

    voxelVolume.setVoxel(12, 57, 35, 49);
    EXPECT_EQ(voxelVolume.voxel(12, 57, 35), 49);
}

TEST(TomoVoxel, createDummyVolume)
{
    // create demo voxel volume with dimensions 80x70x60 (width, height, depth)
    VoxelVolume voxelVolume = createSphericalDemoVolume();
    EXPECT_EQ(voxelVolume.byteCount(), 80*70*60);
    EXPECT_EQ(voxelVolume.width(), 80);

    // Created voxel volume has a sphere, centered in the middle
    // voxels inside the sphere have a gray value of 125, voxels outside 45
    EXPECT_EQ(voxelVolume.voxel(40, 35, 30), 125);
    EXPECT_EQ(voxelVolume.voxel(4, 3, 2), 45);
}

TEST(TomoVoxel, renderVoxelVolume)
{

}

