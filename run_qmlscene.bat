@echo off
setlocal

rem Runs qmlscene for this project
rem Start this from Qt Creator with the following parameters:
rem %{CurrentProject:QT_INSTALL_BINS} %{CurrentProject:Path} %{CurrentProject:BuildPath} %{CurrentDocument:FilePath}

set QT_BIN_PATH=%1
set SRC_PATH=%2
set BUILD_PATH=%3
set QML_FILE=%4

set CURRENT_DIR=%CD%
call %QT_BIN_PATH%\qtenv2.bat
cd /D %CURRENT_DIR%

%QT_BIN_PATH%\qmlscene %QML_FILE%
