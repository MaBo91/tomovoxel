configure_file(tomographyExportFastGrayscale.csv ${RUN_DIR}/tomographyExportFastGrayscale.csv COPYONLY)
configure_file(tomographyExportHQGrayscale.csv ${RUN_DIR}/tomographyExportHQGrayscale.csv COPYONLY)
configure_file(tomographyExportAlpha.csv ${RUN_DIR}/tomographyExportAlpha.csv COPYONLY)
configure_file(tomographyExportHQAlpha.csv ${RUN_DIR}/tomographyExportHQAlpha.csv COPYONLY)

