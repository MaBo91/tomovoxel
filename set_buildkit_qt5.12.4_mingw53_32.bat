@echo off

if NOT DEFINED QTDIR (
	set QTDIR=C:\Qt\5.12.9\mingw73_32
)

if NOT DEFINED QTCREATORDIR (
    set QTCREATORDIR=C:\Qt\Tools\QtCreator
)    
if NOT DEFINED JOM (
	set JOM=%QTCREATORDIR%\bin\jom.exe
)
echo "JOM is set to: %JOM%"
if "%MAKE%"=="" (
	set MAKE=mingw32-make
)
rem PATH-Variable setzen
if NOT DEFINED MINGWDIR (
	set MINGWDIR=C:\Qt\Tools\mingw730_32
)
set PATH=%MINGWDIR%\bin;%QTDIR%\bin;%QTCREATORDIR%\bin;C:\Program Files (x86)\GnuWin32\bin;C:\Program Files\TortoiseSVN\bin;%PATH%

rem CMake
set PATH=C:\Qt\Tools\CMake_64\bin;%PATH%

rem Git
set PATH=C:\Program Files\Git\cmd;%PATH%
