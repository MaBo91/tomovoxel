@echo off
setlocal
set ScriptDir=%~dp0

set SOURCE_DIR=%ScriptDir%
if NOT DEFINED BUILD_DIR (
    set BUILD_DIR=%ScriptDir%\..\build_TomoVoxel
)
if not exist %BUILD_DIR% (
	mkdir %BUILD_DIR%
)

call %ScriptDir%\set_buildkit_qt5.12.4_mingw53_32.bat

cd %BUILD_DIR%
cmake %SOURCE_DIR% -G "MinGW Makefiles"
rem %MAKE% >%ScriptDir%\build.log 2>%ScriptDir%\build_error.log
rem %MAKE% 2>%ScriptDir%\build_error.log
%MAKE%
