@echo off
setlocal
set ScriptDir=%~dp0

set SOURCE_DIR=%ScriptDir%
if NOT DEFINED BUILD_DIR (
    set BUILD_DIR=%ScriptDir%\..\build_TomoVoxel
)

call set_run_paths.bat
echo %PATH%

cd %BUILD_DIR%\bin
DEMO.exe
