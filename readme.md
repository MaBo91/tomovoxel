# Voxel viewer for Pentacam Tomography data

# Build and run

* install Qt 5.12.x, install CMake
* if needed, create your own set_buildkit_xxx.bat and call this in build.bat
* run build.bat, this should build the project using CMake
* if needed, adjust set_run_paths.bat to match your Qt installation
* execute run_demo.bat to run the application

