#pragma once

#include "TomoVoxel_Export.h"

#include "FBOInSGRenderer.h"
#include "TestRenderer.h"
#include "helloShader.h"
#include "VoxelRenderer.h"

#include <QOpenGLFramebufferObject>

namespace voxel {

class TOMOVOXEL_DLL VoxelInFBORenderer :  public QQuickFramebufferObject::Renderer
{
public:
    explicit VoxelInFBORenderer(CameraControls* cameraControls, RenderControls* renderControls);

    void render() override;

    QOpenGLFramebufferObject *createFramebufferObject(const QSize & size) override;

    void calculateFrameRate();

private:
    VoxelRenderer m_voxelRenderer;

    QOpenGLFramebufferObject *m_mainFBO;

    int m_selection;
};

} // namespace voxel
