#include "TestRenderer.h"
#include "math.h"

namespace voxel {

TestRenderer::TestRenderer()
{

}

TestRenderer::~TestRenderer()
{

}

void TestRenderer::render()
{
    m_glFuncs->glDepthMask(true);
    m_glFuncs->glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    m_glFuncs->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    m_glFuncs->glFrontFace(GL_CW);
    m_glFuncs->glCullFace(GL_FRONT);
    m_glFuncs->glEnable(GL_CULL_FACE);
    m_glFuncs->glEnable(GL_DEPTH_TEST);

    QMatrix4x4 modelview;
    modelview.rotate(m_fAngle, 0.0f, 1.0f, 0.0f);
    modelview.rotate(m_fAngle, 1.0f, 0.0f, 0.0f);
    modelview.rotate(m_fAngle, 0.0f, 0.0f, 1.0f);
    modelview.scale(m_fScale);
    modelview.translate(0.0f, -0.2f, 0.0f);

    shaderProgram.bind();
    shaderProgram.setUniformValue(matrixUniform1, modelview);
    draw();
    shaderProgram.release();

    m_glFuncs->glDisable(GL_DEPTH_TEST);
    m_glFuncs->glDisable(GL_CULL_FACE);
    qDebug() << m_fAngle;
    m_fAngle += 1.0;
}

void TestRenderer::init()
{
    m_glFuncs = QOpenGLContext::currentContext()->functions();

    m_glFuncs->glClearColor(0.1f, 0.1f, 0.2f, 1.0f);

    QOpenGLShader *vertexShader = new QOpenGLShader(QOpenGLShader::Vertex, &shaderProgram);
    const char *vs =
            "attribute highp vec4 vertex;\n"
            "attribute mediump vec3 normal;\n"
            "uniform mediump mat4 matrix;\n"
            "varying mediump vec4 color;\n"
            "void main(void)\n"
            "{\n"
            "   vec3 toLight = normalize(vec3(0.0, 0.3, 1.0));\n"
            "   float angle = max(dot(normal, toLight), 0.0);\n"
            "   vec3 col = vec3(1.0, 0.0, 0.0);\n"
            "   color = vec4(col * 0.2 + col * 0.8 * angle, 1.0);\n"
            "   color = clamp(color, 0.0, 1.0);\n"
            "   gl_Position = matrix * vertex;\n"
            "}\n";
    vertexShader->compileSourceCode(vs);

    QOpenGLShader *fragmentShader = new QOpenGLShader(QOpenGLShader::Fragment, &shaderProgram);
    const char *fs =
            "varying mediump vec4 color;\n"
            "void main(void)\n"
            "{\n"
            "   gl_FragColor = color;\n"
            "}\n";
    fragmentShader->compileSourceCode(fs);


    shaderProgram.addShader(vertexShader);
    shaderProgram.addShader(fragmentShader);
    shaderProgram.link();

    vertexAttr1 = shaderProgram.attributeLocation("vertex");
    normalAttr1 = shaderProgram.attributeLocation("normal");
    matrixUniform1 = shaderProgram.uniformLocation("matrix");

    m_glFuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_glFuncs->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    m_fAngle = 0;
    m_fScale = 1;
    createGeometry();
}

void TestRenderer::draw()
{
    shaderProgram.enableAttributeArray(normalAttr1);
    shaderProgram.enableAttributeArray(vertexAttr1);
    shaderProgram.setAttributeArray(vertexAttr1, vertices.constData());
    shaderProgram.setAttributeArray(normalAttr1, normals.constData());
    m_glFuncs->glDrawArrays(GL_TRIANGLES, 0, vertices.size());
    shaderProgram.disableAttributeArray(normalAttr1);
    shaderProgram.disableAttributeArray(vertexAttr1);
}

void TestRenderer::createGeometry()
{
    vertices.clear();
    normals.clear();

    const qreal Pi = 3.14159f;
    const int NumSectors = 3;

    for (int i = 0; i < NumSectors; i++) {
        qreal angle1 = (i * 2 * Pi) / NumSectors;
        qreal x5 = 0.30 * sin(angle1);
        qreal y5 = 0.30 * cos(angle1);
        qreal x6 = 0.20 * sin(angle1);
        qreal y6 = 0.20 * cos(angle1);

        qreal angle2 = ((i + 1) * 2 * Pi) / NumSectors;
        qreal x7 = 0.20 * sin(angle2);
        qreal y7 = 0.20 * cos(angle2);
        qreal x8 = 0.30 * sin(angle2);
        qreal y8 = 0.30 * cos(angle2);

        quad(x5, y5, x6, y6, x7, y7, x8, y8);
        extrude(x6, y6, x7, y7);
        extrude(x8, y8, x5, y5);
    }

    for (int i = 0; i < vertices.size(); i++)
        vertices[i] *= 3.0f;

}

void TestRenderer::quad(qreal x1, qreal y1, qreal x2, qreal y2, qreal x3, qreal y3, qreal x4, qreal y4)
{
    vertices << QVector3D(x1, y1, -0.05f);
    vertices << QVector3D(x2, y2, -0.05f);
    vertices << QVector3D(x4, y4, -0.05f);

    vertices << QVector3D(x3, y3, -0.05f);
    vertices << QVector3D(x4, y4, -0.05f);
    vertices << QVector3D(x2, y2, -0.05f);

    QVector3D n = QVector3D::normal(QVector3D(x2 - x1, y2 - y1, 0.0f), QVector3D(x4 - x1, y4 - y1, 0.0f));

    normals << n;
    normals << n;
    normals << n;

    normals << n;
    normals << n;
    normals << n;

    vertices << QVector3D(x4, y4, 0.05f);
    vertices << QVector3D(x2, y2, 0.05f);
    vertices << QVector3D(x1, y1, 0.05f);

    vertices << QVector3D(x2, y2, 0.05f);
    vertices << QVector3D(x4, y4, 0.05f);
    vertices << QVector3D(x3, y3, 0.05f);

    n = QVector3D::normal(QVector3D(x2 - x4, y2 - y4, 0.0f), QVector3D(x1 - x4, y1 - y4, 0.0f));

    normals << n;
    normals << n;
    normals << n;

    normals << n;
    normals << n;
    normals << n;
}

void TestRenderer::extrude(qreal x1, qreal y1, qreal x2, qreal y2)
{
    vertices << QVector3D(x1, y1, +0.05f);
    vertices << QVector3D(x2, y2, +0.05f);
    vertices << QVector3D(x1, y1, -0.05f);

    vertices << QVector3D(x2, y2, -0.05f);
    vertices << QVector3D(x1, y1, -0.05f);
    vertices << QVector3D(x2, y2, +0.05f);

    QVector3D n = QVector3D::normal(QVector3D(x2 - x1, y2 - y1, 0.0f), QVector3D(0.0f, 0.0f, -0.1f));

    normals << n;
    normals << n;
    normals << n;

    normals << n;
    normals << n;
    normals << n;

}

} // namespace voxel
