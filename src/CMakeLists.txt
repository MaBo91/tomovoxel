cmake_minimum_required (VERSION 3.6.0)
set(LIB_NAME "TomoVoxel")

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

find_package(Qt5 COMPONENTS Core Quick REQUIRED)

add_library(${LIB_NAME} SHARED
    #TomoVoxel_c.cpp
    VoxelVolume.cpp
    VolumeGenerator.cpp
    VoxelInFBORenderer.cpp
    CameraControls.cpp
    RenderControls.cpp
    FBOInSGRenderer.cpp
    #TestRenderer.cpp
    #helloShader.cpp
    VoxelRenderer.cpp
    qml.qrc
)

include (GenerateExportHeader)
GENERATE_EXPORT_HEADER (${LIB_NAME}
        BASE_NAME ${LIB_NAME}
        EXPORT_MACRO_NAME TOMOVOXEL_DLL
        EXPORT_FILE_NAME ${CMAKE_BINARY_DIR}/exports/${LIB_NAME}_Export.h
        STATIC_DEFINE ${LIB_NAME}_BUILT_AS_STATIC
)

target_compile_definitions(${LIB_NAME} PUBLIC ${LIB_NAME}_EXPORTS=1)
target_include_directories(${LIB_NAME} PUBLIC ${CMAKE_BINARY_DIR}/exports ${CMAKE_CURRENT_SOURCE_DIR})

target_link_libraries(${LIB_NAME}
    Qt5::Core
    Qt5::Quick
)

set(DEMO demo_${LIB_NAME})

add_executable(DEMO
    ShowVoxelVolume.cpp
)

target_link_libraries(DEMO
    ${LIB_NAME}
)
