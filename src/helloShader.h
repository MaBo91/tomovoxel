#pragma once

#include "TomoVoxel_Export.h"

#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions>

class TOMOVOXEL_DLL helloShader
{
public:
    helloShader();

    void init();
    void render();

private:
    void draw();

    QOpenGLShaderProgram m_program;
    QOpenGLFunctions *m_glFuncs;

    int vertexLocation;
    int matrixLocation;
};

