#include <CameraControls.h>

#include <QLoggingCategory>
#include <QVector3D>
#include "windows.h"

#include "math.h"

static Q_LOGGING_CATEGORY(LC, "de.oculus.penta.CameraControls", QtInfoMsg)

namespace voxel {

    CameraControls::CameraControls()
    {

    }

    void CameraControls::init()
    {
        m_cameraPosX = 3.0f;
        m_cameraPosY = 0.0f;
        m_cameraPosZ = -1.0f;

        m_verticalAngle = 40;
        m_Angle = 0.0f;
        m_radius = 3.0f;

        m_offsetX = 0.0f;
        m_offsetY = 0.0f;

        m_alpha = 10.0f;
        m_beta = -100.0f;
        m_gamma = 265.0f;

        calcMVP();
    }

    void CameraControls::calcMVP()
    {
        QMatrix4x4 model;
        QMatrix4x4 view;
        QMatrix4x4 projection;

        view.lookAt(
                    QVector3D(m_cameraPosX, m_cameraPosY, m_cameraPosZ), // Camera is at (0, 0, 3) in World-Space
                    QVector3D(0.0, 0.0, 0.0), // and looks at the origin
                    QVector3D(0.0, 1.0, 0.0)); // Up-Vector

        projection.perspective(m_verticalAngle, 800 / 600, 1.0, 100.0); // 45° Field of View, 800/600 Ratio, display range: 1 unit <-> 100 units

        QMatrix4x4 rotation;
        QMatrix4x4 translation;
        QMatrix4x4 scaling;

        // no axis aligned rotation, rotation around a specific point p = (0.5f, 0.5f, 0.5f),
        // the center of the unit cube: first translate to origin of coordinate system, then rotate, then translate back
        rotation.translate(QVector3D(0.5f, 0.5f, 0.5f)); // Translate to origin
        rotation = calcYaw() * calcPitch() * calcRoll(); // perform rotation using euler angles
        rotation.translate(QVector3D(-0.5f, -0.5f, -0.5f));  // translate back

        translation.translate(QVector3D(0.0f, 0.0f, 0.0f)); // no translation
        scaling.scale(QVector3D(1.0f, 1.0f, 1.0f)); // no scaling

        // new model matrix consists of translation, rotation and scaling matrices
        model = rotation * translation * scaling;

        m_mvp = (projection * view * model);
    }

    QMatrix4x4 CameraControls::mvp()
    {
        return m_mvp;
    }

    float CameraControls::offsetX() const
    {
        return m_offsetX;
    }

    void CameraControls::setOffsetX(float offset)
    {
        if(offset != m_offsetX)
            m_offsetX = offset;
        emit offsetXChanged();
    }

    float CameraControls::offsetY() const
    {
        return m_offsetY;
    }

    void CameraControls::setOffsetY(float offset)
    {
        if(offset != m_offsetY)
            m_offsetY = offset;
        emit offsetYChanged();
    }

    void CameraControls::updateMVP()
    {
        m_gamma += -m_offsetX;
        m_beta += -m_offsetY;
        calcMVP();
    }

    void CameraControls::zoomIn()
    {
        m_verticalAngle -= 1.0f;
        calcMVP();
    }

    void CameraControls::zoomOut()
    {
        m_verticalAngle += 1.0f;
        calcMVP();
    }

    void CameraControls::rotateLeft()
    {
        for (int i = 0; i < 60; i++) {
            m_Angle -= 0.1f;
            m_cameraPosX = sin(m_Angle) * m_radius;
            m_cameraPosZ = cos(m_Angle) * m_radius;
            calcMVP();
            Sleep(80);
        }
    }

    void CameraControls::rotateRight()
    {
        for (int i = 0; i < 60; i++) {
            m_Angle += 0.1f;
            m_cameraPosX = sin(m_Angle) * m_radius;
            m_cameraPosZ = cos(m_Angle) * m_radius;
            calcMVP();
            Sleep(80);
        }
    }

    void CameraControls::increaseYaw()
    {
        m_alpha += 5;
        calcMVP();
        qDebug() << "'Increase Yaw' button clicked, new value of m_alpha" << m_alpha;
    }

    void CameraControls::decreaseYaw()
    {
        m_alpha -= 5;
        calcMVP();
        qDebug() << "'Decrease Yaw' button clicked, new value of m_alpha" << m_alpha;
    }

    void CameraControls::increasePitch()
    {
        m_beta += 5;
        calcMVP();
        qDebug() << "'Increase Pitch' button clicked, new value of m_beta" << m_beta;
    }

    void CameraControls::decreasePitch()
    {
        m_beta -= 5;
        calcMVP();
        qDebug() << "'Increase Pitch' button clicked, new value of m_beta" << m_beta;
    }

    void CameraControls::increaseRoll()
    {
        m_gamma += 5;
        calcMVP();
        qDebug() << "'Increase Roll' button clicked, new value of m_gamma" << m_gamma;
    }

    void CameraControls::decreaseRoll()
    {
        m_gamma -= 5;
        calcMVP();
        qDebug() << "'Increase Roll' button clicked, new value of m_gamma" << m_gamma;
    }

    void CameraControls::resetView()
    {
        init();
    }

    QMatrix4x4 CameraControls::calcYaw()
    {
        QMatrix4x4 yaw;
        QVector3D rotationAxisZ = QVector3D(0.0f, 0.0f, 1.0f);
        yaw.rotate(m_alpha, rotationAxisZ);
        return yaw;
    }

    QMatrix4x4 CameraControls::calcPitch()
    {
        QMatrix4x4 pitch;
        QVector3D rotationAxisY = QVector3D(0.0f, 1.0f, 0.0f);
        pitch.rotate(m_beta, rotationAxisY);
        return pitch;
    }

    QMatrix4x4 CameraControls::calcRoll()
    {
        QMatrix4x4 roll;
        QVector3D rotationAxisX = QVector3D(1.0f, 0.0f, 0.0f);
        roll.rotate(m_gamma, rotationAxisX);
        return roll;
    }

} // namespace voxel
