#pragma once

#include "TomoVoxel_Export.h"

#include <QByteArray>

namespace voxel {

class TOMOVOXEL_DLL VoxelVolume
{
public:
    VoxelVolume();
    VoxelVolume(int width, int height, int depth);

    int voxelVolumeSize() const;
    int width() const;
    int height() const;
    int depth() const;

    const char *data() const;

    int byteCount() const;
    int offset(int x, int y, int z) const;

    void setVoxel(int x, int y, int z, uchar value);
    uchar voxel(int x, int y, int z) const;

private:
    int m_width, m_height, m_depth;
    QByteArray m_values;
};

} // namespace voxel
