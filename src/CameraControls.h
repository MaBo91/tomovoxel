#pragma once

#include "TomoVoxel_Export.h"

#include <QMatrix4x4>

namespace voxel {

const float DEG2RAD = 3.141593f / 180.0f;
const float RAD2DEG = 180.0f / 3.141593f;

class TOMOVOXEL_DLL CameraControls : public QObject
{
    Q_OBJECT
    Q_PROPERTY(float offsetX READ offsetX WRITE setOffsetX NOTIFY offsetXChanged)
    Q_PROPERTY(float offsetY READ offsetY WRITE setOffsetY NOTIFY offsetYChanged)

public:
    CameraControls();

    void init();
    void calcMVP();

    QMatrix4x4 mvp();

    float offsetX() const;
    void setOffsetX(float offset);

    float offsetY() const;
    void setOffsetY(float offset);

    Q_INVOKABLE void updateMVP();

    Q_INVOKABLE void zoomIn();
    Q_INVOKABLE void zoomOut();

    Q_INVOKABLE void rotateLeft();
    Q_INVOKABLE void rotateRight();

    Q_INVOKABLE void increaseYaw();
    Q_INVOKABLE void decreaseYaw();

    Q_INVOKABLE void increasePitch();
    Q_INVOKABLE void decreasePitch();

    Q_INVOKABLE void increaseRoll();
    Q_INVOKABLE void decreaseRoll();

    Q_INVOKABLE void resetView();

    QMatrix4x4 calcYaw();
    QMatrix4x4 calcPitch();
    QMatrix4x4 calcRoll();

signals:
    void offsetXChanged();
    void offsetYChanged();

private:
    QMatrix4x4 m_model, m_view, m_projection, m_mvp;

    float m_cameraPosX, m_cameraPosY, m_cameraPosZ;

    float m_verticalAngle;
    float m_Angle;
    int m_radius;

    float m_offsetX, m_offsetY;

    float m_alpha, m_beta, m_gamma;
};

} // namespace voxel
