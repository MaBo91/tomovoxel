#include "FBOInSGRenderer.h"

#include "TestRenderer.h"
#include "helloShader.h"
#include "VoxelRenderer.h"
#include "CameraControls.h"
#include "RenderControls.h"
#include "VoxelInFBORenderer.h"

#include <QLoggingCategory>
#include <QOpenGLFramebufferObject>

static Q_LOGGING_CATEGORY(LC, "de.oculus.penta.voxelinfborenderer", QtInfoMsg)

namespace voxel {

FBOInSGRenderer::FBOInSGRenderer() : m_pCameraControls(new CameraControls),
    m_pRenderControls(new RenderControls)
{

}

QQuickFramebufferObject::Renderer *FBOInSGRenderer::createRenderer() const
{
    return new VoxelInFBORenderer(m_pCameraControls, m_pRenderControls);
}

QObject *FBOInSGRenderer::cameraControls() const
{
    return m_pCameraControls;
}

QObject *FBOInSGRenderer::renderControls() const
{
    return m_pRenderControls;
}

} // namespace voxel
