#include "RenderControls.h"

#include <QLoggingCategory>
#include "qdebug.h"

static Q_LOGGING_CATEGORY(LC, "de.oculus.penta.RenderControls", QtInfoMsg)

namespace voxel {

RenderControls::RenderControls()
{
     m_sampleStepSize = 0.01f;
     m_overexposure = 1.0f; // 1.0: no overexposure, higher values increase overexposure
     m_haveMaterial = false;
     m_maxIntensityProjection = false;
     m_alphaBlending = false;
     m_volumeSelect = true;
     m_jittering = false;
     m_materialThreshold = 0.5f;
     m_alpha = 1.0f;
     m_gamma = 1.0f;
     m_jitterFactor = 1.0f;
}

float RenderControls::sampleStepSize() const
{
    return m_sampleStepSize;
}

void RenderControls::setSampleStepSize(float sampleStepSize)
{
    if(sampleStepSize != m_sampleStepSize)
        m_sampleStepSize = sampleStepSize;
    emit sampleStepSizeChanged();
}

float RenderControls::overexposure() const
{
    return m_overexposure;
}

void RenderControls::increaseSampleSteps()
{
    if(m_sampleStepSize > 0.01f)
    m_sampleStepSize -= 0.01f;
    qDebug() << "Sample step size = " << m_sampleStepSize;
}

void RenderControls::decreaseSampleSteps()
{
    m_sampleStepSize += 0.01f;
    qDebug()  << "Sample step size = " << m_sampleStepSize;
}

void RenderControls::increaseOverexposure()
{
    m_overexposure += 0.1f;
    qDebug() << "Overexposure = " << m_overexposure;
}

void RenderControls::decreaseOverexposure()
{
    if(m_overexposure > 0.1f)
    m_overexposure -= 0.1f;
    qDebug() << "Overexposure = " << m_overexposure;
}

void RenderControls::reset()
{
    m_sampleStepSize = 0.01f;
    m_overexposure = 1.0f;
}

void RenderControls::setOverexposure(float overexposure)
{
    if(overexposure != m_overexposure)
        m_overexposure = overexposure;
    emit overexposureChanged();
}

float RenderControls::materialThreshold() const
{
    return m_materialThreshold;
}

void RenderControls::setMaterialThreshold(float materialThreshold)
{
    if(materialThreshold != m_materialThreshold)
        m_materialThreshold = materialThreshold;
    emit materialThresholdChanged();
}

float RenderControls::alpha() const
{
    return m_alpha;
}

void RenderControls::setAlpha(float alpha)
{
    if(alpha != m_alpha)
        m_alpha = alpha;
    emit alphaChanged();
}

float RenderControls::gamma() const
{
    return m_gamma;
}

void RenderControls::setGamma(float gamma)
{
    if(gamma != m_gamma)
        m_gamma = gamma;
    emit gammaChanged();
}

float RenderControls::jitterFactor() const
{
    return m_jitterFactor;
}

void RenderControls::setJitterFactor(float factor)
{
    if(factor != m_jitterFactor)
        m_jitterFactor = factor;
    emit jitterFactorChanged();
}

bool RenderControls::haveMaterial() const
{
    return m_haveMaterial;
}

bool RenderControls::maxIntensityProjection() const
{
    return m_maxIntensityProjection;
}

bool RenderControls::alphaBlending() const
{
    return m_alphaBlending;
}

bool RenderControls::toggleHaveMaterial()
{
    m_haveMaterial = !m_haveMaterial;
    return m_haveMaterial;
}

bool RenderControls::toggleMaxIntensityProjection()
{
    m_maxIntensityProjection = !m_maxIntensityProjection;
    return m_maxIntensityProjection;
}

bool RenderControls::toggleAlphaBlending()
{
    m_alphaBlending =! m_alphaBlending;
    return m_alphaBlending;
}

bool RenderControls::volumeSelect() const
{
    return m_volumeSelect;
}

bool RenderControls::toggleVolumeSelect()
{
    m_volumeSelect =! m_volumeSelect;
    return m_volumeSelect;
}

bool RenderControls::jittering() const
{
    return m_jittering;
}

bool RenderControls::toggleJittering()
{
    m_jittering =! m_jittering;
    return m_jittering;
}

} // namespace voxel
