#include "VoxelRenderer.h"

#include "VolumeGenerator.h"

#include <QLoggingCategory>
#include <QMatrix4x4>
#include <cmath>
#include <QElapsedTimer>

static Q_LOGGING_CATEGORY(LC, "de.oculus.penta.voxelrenderer", QtInfoMsg)

namespace voxel {

    // 1 Cube = 2 Triangles * 6 Faces = 12 Triangles consisting of 12*3 Vertices
    static const GLfloat unitCubeVertexandColorData[] = {

        // front right
        0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 1.0f,
        0.0f, 0.0f, 1.0f,

        0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 1.0f,

        // front left
        0.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 0.0f,

        0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        // top
        0.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 1.0f,
        0.0f, 1.0f, 1.0f,

        0.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 1.0f,

        // bottom
        0.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f,

        0.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 0.0f,

        // back right
        0.0f, 0.0f, 1.0f,
        0.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,

        0.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 0.0f, 1.0f,

        // back left
        1.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 1.0f,

        1.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 0.0f,
    };

    VoxelRenderer::VoxelRenderer(CameraControls *cameraControls, RenderControls *renderControls) :
        m_cameraControls(cameraControls), m_renderControls(renderControls)
    {

    }

    void VoxelRenderer::init(const QSize &size)
    {
        // use OpenGL 3.1 functions
        m_glFuncs = QOpenGLContext::currentContext()->versionFunctions<QOpenGLFunctions_3_1>();

        // set camera parameters to default
        m_cameraControls->init();

        if(!m_isInitialized)
        {
            // start timer for measurement of volume loading & processing time
            QElapsedTimer *timer = new QElapsedTimer;
            timer->start();

            // create volume (fast)
            //VoxelVolume voxelVolumeGrayValue = createPentacamDataFastVolume("tomographyExportFastGrayscale.csv");
            //VoxelVolume voxelVolumeAlpha = createPentacamDataFastVolume("tomographyExportAlpha.csv");

            // create volume (HQ)
            VoxelVolume voxelVolumeGrayValue = createPentacamDataHQVolume("tomographyExportHQGrayscale.csv");
            VoxelVolume voxelVolumeAlpha = createPentacamDataHQVolume("tomographyExportHQAlpha.csv");
            qCInfo(LC) << "Created Voxel-Volume";

            // save volume dataset in graphic memory (QByteArray -> 3D-Texture)
            m_voxelDataGrayValueTexture3D = voxelDataTo3DTexture(voxelVolumeGrayValue); // using Qt OpenGL
            m_voxelDataAlphaTexture3D = voxelDataTo3DTexture(voxelVolumeAlpha);

            qCInfo(LC) << "Voxeldata stored in 3D texture with dimensions: width =" << m_voxelDataGrayValueTexture3D->width() << " height =" << m_voxelDataGrayValueTexture3D->height() << " depth =" << m_voxelDataGrayValueTexture3D->depth();
            qCInfo(LC) << "Loading and storing volume in 3D texture took" << timer->elapsed() << " ms.";

            // texture with random uchars to substitute aliasing by noise
            createJitterTexture(m_jitterTextureSize);
            qCInfo(LC) << "Jitter-Texture created!";

            // add and link shader programs of selected rendering program
            m_programUnitCubeRendering.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/VertexShaderBoundingBox.vert");
            m_programUnitCubeRendering.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/FragmentShaderBoundingBox.frag");
            m_programUnitCubeRendering.link();
            if (m_programUnitCubeRendering.isLinked() == false) qCCritical(LC) << "Linking shaderprogram: 'm_programUnitCubeRendering' failed";

            m_mainShaderProgram.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/VertexShaderRaycasting.vert");
            m_mainShaderProgram.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/FragmentShaderRaycasting.frag");
            m_mainShaderProgram.link();
            if (m_mainShaderProgram.isLinked() == false) qCCritical(LC) << "Linking shaderprogram: 'm_mainShaderProgram' failed";

            m_isInitialized = true;
        }

        setImageSize(size); // set correct size for 2D texture init
        initUnitCubeTextures(); // init 2D textures for front/backface texture with proper format
        qCInfo(LC) << "Initialized textures for unit cube bounding box";
    }

    void VoxelRenderer::render()
    {
        m_glFuncs->glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        m_glFuncs->glDepthMask(true);
        m_glFuncs->glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
        m_glFuncs->glEnable(GL_DEPTH_TEST);

        // bind textures to texture units to get access in fragment-shader
        m_backfaceTexture2D->bind(1);
        m_frontfaceTexture2D->bind(2);
        m_voxelDataGrayValueTexture3D->bind(3);
        m_voxelDataAlphaTexture3D->bind(4);
        m_jitterTexture2D->bind(5);

        m_mainShaderProgram.bind();
        m_mainShaderProgram.setUniformValue("backfaceTexture", 1);
        m_mainShaderProgram.setUniformValue("frontfaceTexture", 2);
        m_mainShaderProgram.setUniformValue("volumeTexture", 3);
        m_mainShaderProgram.setUniformValue("volumeAlphaTexture", 4);
        m_mainShaderProgram.setUniformValue("jitterTexture", 5);

        // set render controls parameters as uniforms to use them in fragment-shader
        m_mainShaderProgram.setUniformValue("sampleStepSize", m_renderControls->sampleStepSize());
        m_mainShaderProgram.setUniformValue("overexposure" , m_renderControls->overexposure());
        m_mainShaderProgram.setUniformValue("enableHaveMaterial", m_renderControls->haveMaterial());
        m_mainShaderProgram.setUniformValue("materialThreshold", m_renderControls->materialThreshold());
        m_mainShaderProgram.setUniformValue("enableMaxIntensity", m_renderControls->maxIntensityProjection());
        m_mainShaderProgram.setUniformValue("enableAlphaBlending", m_renderControls->alphaBlending());
        m_mainShaderProgram.setUniformValue("enableJittering", m_renderControls->jittering());
        m_mainShaderProgram.setUniformValue("volumeSelect", m_renderControls->volumeSelect());
        m_mainShaderProgram.setUniformValue("alpha", m_renderControls->alpha());
        m_mainShaderProgram.setUniformValue("gamma", m_renderControls->gamma());
        m_mainShaderProgram.setUniformValue("jitterTexSize", m_jitterTextureSize);
        m_mainShaderProgram.setUniformValue("jitterFactor", m_renderControls->jitterFactor());

        // handles for vertex and uv-coordinates
        m_vertexLocationID = m_mainShaderProgram.attributeLocation("vertex");
        m_UVsID = m_mainShaderProgram.attributeLocation("vertexUV");

        // define vertices for the surface the rendering result is shown on
        static const GLfloat rectData[] = {
            -1.0f, -1.0f, 0.0f,
            1.0f, -1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,

            -1.0f, -1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,
            -1.0f, 1.0f, 0.0f,
        };

        // define uv-coordinates to sample front/backface textures of unit-cube in fragment-shader
        static const GLfloat UVs[] = {
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,

            0.0f, 0.0f,
            1.0f, 1.0f,
            1.0f, 0.0f,
        };

        m_mainShaderProgram.enableAttributeArray(m_vertexLocationID);
        m_mainShaderProgram.enableAttributeArray(m_UVsID);
        m_mainShaderProgram.setAttributeArray(m_vertexLocationID, rectData, 3);
        m_mainShaderProgram.setAttributeArray(m_UVsID, UVs, 2);

        m_glFuncs->glDrawArrays(GL_TRIANGLES, 0, 2*3); // draw call

        m_mainShaderProgram.disableAttributeArray(m_vertexLocationID);
        m_mainShaderProgram.disableAttributeArray(m_UVsID);

        m_mainShaderProgram.release();
        m_mainShaderProgram.removeAllShaders();

        m_backfaceTexture2D->release(1);
        m_frontfaceTexture2D->release(2);
        m_voxelDataGrayValueTexture3D->release(3);
        m_voxelDataAlphaTexture3D->release(4);
        m_jitterTexture2D->release(5);

        m_glFuncs->glActiveTexture(GL_TEXTURE0);
        m_glFuncs->glDisable(GL_DEPTH_TEST);
    }

    void VoxelRenderer::setImageSize(const QSize &size)
    {
        m_imageWidth = size.width();
        m_imageHeight = size.height();
    }

    void VoxelRenderer::initUnitCubeTextures()
    {
        // create 2D QOpenGLTextures with size of renderer-window and proper format
        m_backfaceTexture2D = new QOpenGLTexture(QOpenGLTexture::Target2D);
        m_backfaceTexture2D->setMinMagFilters(QOpenGLTexture::Nearest, QOpenGLTexture::Nearest);
        m_backfaceTexture2D->create();
        m_backfaceTexture2D->setSize(m_imageWidth, m_imageHeight, 0);
        m_backfaceTexture2D->setFormat(QOpenGLTexture::RGBAFormat);
        m_backfaceTexture2D->allocateStorage();
        m_backfaceTexture2D->setData(QOpenGLTexture::RGBA, QOpenGLTexture::Float32, (void*) nullptr);

        m_frontfaceTexture2D = new QOpenGLTexture(QOpenGLTexture::Target2D);
        m_frontfaceTexture2D->setMinMagFilters(QOpenGLTexture::Nearest, QOpenGLTexture::Nearest);
        m_frontfaceTexture2D->create();
        m_frontfaceTexture2D->setSize(m_imageWidth, m_imageHeight, 0);
        m_frontfaceTexture2D->setFormat(QOpenGLTexture::RGBAFormat);
        m_frontfaceTexture2D->allocateStorage();
        m_frontfaceTexture2D->setData(QOpenGLTexture::RGBA, QOpenGLTexture::Float32, (void*) nullptr);
    }

    QOpenGLTexture *VoxelRenderer::voxelDataTo3DTexture(const VoxelVolume &voxelVolume)
    {
        // create 3D texture with data of voxelVolume, which is saved in QByteArray
        QOpenGLTexture *voxelVolumeTexture = new QOpenGLTexture(QOpenGLTexture::Target3D);
        voxelVolumeTexture->setMinMagFilters(QOpenGLTexture::Linear, QOpenGLTexture::Linear);
        voxelVolumeTexture->create();
        voxelVolumeTexture->setSize(voxelVolume.width(), voxelVolume.height(), voxelVolume.depth());
        voxelVolumeTexture->setFormat(QOpenGLTexture::LuminanceFormat);
        voxelVolumeTexture->allocateStorage();
        voxelVolumeTexture->setData(QOpenGLTexture::Luminance, QOpenGLTexture::UInt8, voxelVolume.data());
        return voxelVolumeTexture;
    }

    void VoxelRenderer::createJitterTexture(int size)
    {
        unsigned int jitter[size * size];
        int elementCount = sizeof (jitter) / sizeof (jitter[0]);

        // fill with random numbers to create noise
        for(int i = 0; i < elementCount; ++i)
        {
            jitter[i] = std::rand();
        }

        m_jitterTexture2D = new QOpenGLTexture(QOpenGLTexture::Target2D);
        m_jitterTexture2D->setMinMagFilters(QOpenGLTexture::Nearest, QOpenGLTexture::Nearest);
        m_jitterTexture2D->setWrapMode(QOpenGLTexture::Repeat);
        m_jitterTexture2D->create();
        m_jitterTexture2D->setSize(size, size);
        m_jitterTexture2D->setFormat(QOpenGLTexture::LuminanceFormat);
        m_jitterTexture2D->allocateStorage();
        m_jitterTexture2D->setData(QOpenGLTexture::Luminance, QOpenGLTexture::UInt8, jitter);
    }

    void VoxelRenderer::renderUnitCubeToTexture(VoxelRenderer::UnitCubeFaces faces)
    {
        // create framebuffer to perform offscreen rendering
        GLuint fbo;
        m_glFuncs->glGenFramebuffers(1, &fbo);
        m_glFuncs->glBindFramebuffer(GL_FRAMEBUFFER, fbo);

        // render to framebuffer instead render to screen
        if(faces == BACKFACES) {
            m_glFuncs->glFramebufferTexture2D(
                        GL_FRAMEBUFFER,
                        GL_COLOR_ATTACHMENT0,
                        GL_TEXTURE_2D,
                        m_backfaceTexture2D->textureId(),
                        0);
        }

        else if (faces == FRONTFACES) {
            m_glFuncs->glFramebufferTexture2D(
                        GL_FRAMEBUFFER,
                        GL_COLOR_ATTACHMENT0,
                        GL_TEXTURE_2D,
                        m_frontfaceTexture2D->textureId(),
                        0);
        }

        GLenum e = m_glFuncs->glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (e != GL_FRAMEBUFFER_COMPLETE)
            printf("There is a problem with the FBO\n");

        // render unit cube with selected faces
        m_glFuncs->glBindFramebuffer(GL_FRAMEBUFFER, fbo);
        m_glFuncs->glViewport(0, 0, m_imageWidth, m_imageHeight);

        renderUnitCube(faces);

        m_glFuncs->glDeleteFramebuffers(1, &fbo);
    }

    void VoxelRenderer::renderUnitCube(VoxelRenderer::UnitCubeFaces faces)
    {
        // set transparent background, enable depth buffering and face culling
        m_glFuncs->glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        m_glFuncs->glDepthMask(true);
        m_glFuncs->glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
        m_glFuncs->glEnable(GL_DEPTH_TEST);
        m_glFuncs->glEnable(GL_CULL_FACE);

        // if function parameter = FRONTFACES, render front faces
        if (faces == FRONTFACES) m_glFuncs->glCullFace(GL_FRONT);
        else if (faces == BACKFACES) m_glFuncs->glCullFace(GL_BACK); // else render back faces

        // get current MVP matrix
        QMatrix4x4 mvp = m_cameraControls->mvp();

        m_programUnitCubeRendering.bind();

        // send used variables (attributes/uniforms) to shader and define how to interpret this data
        m_programUnitCubeRendering.setUniformValue(m_matrixLocationID, mvp);
        m_vertexLocationID = m_programUnitCubeRendering.attributeLocation("vertex");
        m_matrixLocationID = m_programUnitCubeRendering.uniformLocation("MVP");

        m_programUnitCubeRendering.enableAttributeArray(m_vertexLocationID);
        m_programUnitCubeRendering.setAttributeArray(m_vertexLocationID, unitCubeVertexandColorData, 3);

        m_glFuncs->glDrawArrays(GL_TRIANGLES, 0, 12*3); // draw call

        m_programUnitCubeRendering.disableAttributeArray(m_vertexLocationID);

        m_programUnitCubeRendering.release();
        m_programUnitCubeRendering.removeAllShaders();

        m_glFuncs->glDisable(GL_DEPTH_TEST);
    }

} // namespace voxel
