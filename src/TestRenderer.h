#pragma once

#include "TomoVoxel_Export.h"

#include <QVector>
#include <QVector3D>
#include <QMatrix4x4>
#include <QOpenGLShaderProgram>
#include <QOpenGLFunctions>

namespace voxel {

class TOMOVOXEL_DLL TestRenderer
{
public:
    TestRenderer();
    ~TestRenderer();

    void render();
    void init();

private:
    float m_fAngle;
    float m_fScale;

    void draw();
    void createGeometry();
    void quad(qreal x1, qreal y1, qreal x2, qreal y2, qreal x3, qreal y3, qreal x4, qreal y4);
    void extrude(qreal x1, qreal y1, qreal x2, qreal y2);

    QVector<QVector3D> vertices;
    QVector<QVector3D> normals;
    QOpenGLShaderProgram shaderProgram;
    QOpenGLFunctions *m_glFuncs;

    int vertexAttr1;
    int normalAttr1;
    int matrixUniform1;
};

} // namespace voxel
