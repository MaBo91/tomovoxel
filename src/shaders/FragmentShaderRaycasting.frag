#version 330 core
in vec2 UV; // interpolated UV coordinates
in vec4 gl_FragCoord;

// texture sampler
uniform sampler2D backfaceTexture;
uniform sampler2D frontfaceTexture;
uniform sampler3D volumeTexture;
uniform sampler3D volumeAlphaTexture;
uniform sampler2D jitterTexture;

// render params
uniform float sampleStepSize;
uniform float overexposure;
uniform bool volumeSelect;
uniform bool enableMaxIntensity;
uniform bool enableHaveMaterial;
uniform bool enableAlphaBlending;
uniform bool enableJittering;
uniform float materialThreshold;
uniform float alpha;
uniform float gamma;
uniform int jitterTexSize;
uniform float jitterFactor;

const float maxVectorLength = length(vec3(1.0, 1.0, 1.0)); // used for exposure-normalization

out vec4 color; // output for frame buffer

vec4 colorTransferFunction(float intensity)
{
    vec3 high = vec3(1.0, 1.0, 1.0);
    vec3 low = vec3(0.0, 0.0, 0.0);

    float alphaValue = alpha * ((exp(intensity) - 1.0) / (exp(1.0) - 1.0));

    return vec4(intensity * high + (1.0 - intensity) * low, alphaValue);
}

void main() {

    // RAY SET UP
    // get entry and exit point of ray by sampling unit cube's front and backface texture
    vec3 exitRayPos = texture2D(backfaceTexture, UV).rgb;
    vec3 startRayPos = texture2D(frontfaceTexture, UV).rgb;

    // calculate traverse vector and ray length (used for loop stop criterion)
    vec3 traverseVector = exitRayPos - startRayPos;
    float maxRayLenght = length(traverseVector);

    if(enableAlphaBlending) {
        color = vec4(0.0, 0.45, 0.75, 1.0).rgba;  // background
    }

    else {
        color = vec4(0.0, 0.0, 0.0, 1.0).rgba;  // background
    }

    if (maxRayLenght > 0.0) { // for rays through the volume

        // compute step vector (used for sampling voxel volume)
        vec3 normalizedRay = normalize(traverseVector);
        vec3 stepVector = normalizedRay * sampleStepSize;

        // optimization: offset in ray starting position along ray direction to reduce wood-grain artifacts
        if(enableJittering) {
            float offset = texture2D(jitterTexture, gl_FragCoord.xy/jitterTexSize).r * jitterFactor;
            startRayPos += offset * stepVector;
            maxRayLenght = maxRayLenght - (offset * sampleStepSize);
        }

        // RAY TRAVERSAL
        // initialize accumulation variables
        float accumulatedGrayValue = 0.0;
        float currentRayLenght = 0.0;
        float maxIntensity = 0.0;

        // front-to-back raycasting: start at entry position
        vec3 currentRayPosition = startRayPos;
        bool haveMaterial = false;

        // ray traversal loop: end if max length exeeded
        while(currentRayLenght < maxRayLenght)
        {
            float grayValueSample;
            float alphaSample;

            if(volumeSelect) {
                // get sample from 3D grayvalue texture
                grayValueSample = texture3D(volumeTexture, currentRayPosition).r;
                alphaSample = texture3D(volumeTexture, currentRayPosition).a * sampleStepSize;
            }

            else {
                // get sample from 3D alpha texture
                grayValueSample = texture3D(volumeAlphaTexture, currentRayPosition).r;
                alphaSample = texture3D(volumeAlphaTexture, currentRayPosition).a * sampleStepSize;
            }

            if(enableAlphaBlending)
            {
                if (grayValueSample > materialThreshold) {
                    vec4 c = colorTransferFunction(grayValueSample);

                    // Alpha-Blending
                    color.rgb = c.a * c.rgb + (1 - c.a) * color.a * color.rgb;
                    color.a = c.a + (1 - c.a) * color.a;
                }
            }

            else if(enableMaxIntensity)
            {
                if(grayValueSample > materialThreshold) {
                    if(grayValueSample > maxIntensity) {
                        maxIntensity = grayValueSample;
                    }
                }
            }

            else {
                // sampled gray value is between 0 and 1, accumulate only values greater <materialThreshold>
                // is assumed that the ray hits material
                if (grayValueSample > materialThreshold) {
                    accumulatedGrayValue += grayValueSample;
                    haveMaterial = true;
                }
            }

            // compute next sample position
            currentRayPosition += stepVector;

            // compute ray length traversed
            currentRayLenght += sampleStepSize;
        }

        // normalize gray value: 1.0 -> no overexposure, higher values increase overexposure
        float normFactor = overexposure / (maxVectorLength / sampleStepSize);
        float normalizedGrayValue = normFactor * accumulatedGrayValue;

        // gamma correction
        normalizedGrayValue = pow(normalizedGrayValue, (1.0/gamma));
        maxIntensity = pow(maxIntensity, (1.0/gamma));

        if(enableAlphaBlending) {
            color.rgb = color.a * color.rgb + (1 - color.a) * pow(color.rgb, vec3(gamma)).rgb;
            color.a = 1.0;
        }

        else if(enableMaxIntensity) {
            color = color + vec4(maxIntensity, maxIntensity, maxIntensity, 1.0);
        }

        // add sampled color for current pixel to background color
        else {
            color = color + vec4(normalizedGrayValue, normalizedGrayValue, normalizedGrayValue, 1.0).rgba;
        }

        // if ray hits material, add some color to the green channel
        if (enableHaveMaterial) {
            if(haveMaterial) {
                color = color + vec4(0.0, 0.5, 0.0, 0.0).rgba;
            }
        }
    }
}

