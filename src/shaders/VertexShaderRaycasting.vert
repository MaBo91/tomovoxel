#version 330 core
in vec3 vertex; // input: vertex data of quadratic window
in vec2 vertexUV; // input: UV coordinates for 2D texture sampling

out vec2 UV; // output: interpolated UV coordinates for fragment-shader

void main() {
    UV = vertexUV; // pass interpolated UV coordinates to fragment-shader
    gl_Position = vec4(vertex, 1.0);
}
