#version 330
in vec3 vertexData; // input: interpolated vertex data from vertex shader
out vec4 color; // output: final pixel color

void main()
{
    color = vec4(vertexData, 1.0).rgba; // map unit cube vertex data to color channel
}
