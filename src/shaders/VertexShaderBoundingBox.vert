#version 330
attribute vec3 vertex; // input: vertex data of 3D unit cube
uniform mat4 MVP; // input: Model-View-Projection-Matrix

out vec3 vertexData; // output: vertex data for fragment-shader

void main()
{
    vertexData = vertex; // pass interpolated vertex data to fragment-shader
    gl_Position = MVP * vec4(vertex, 1.0); // calculate position of each vertex with MVP
}
