#include "VolumeGenerator.h"

#include <cmath>
#include <QByteArrayList>
#include <QFile>
#include <QLoggingCategory>

namespace voxel {

static Q_LOGGING_CATEGORY(LC, "de.oculus.penta.VolumeGenerator", QtInfoMsg)

// Demo-Volume: Sphere
VoxelVolume createSphericalDemoVolume(int nx, int ny, int nz)
{
    double centerX = nx/2;
    double centerY = ny/2;
    double centerZ = nz/2;
    double exponent = 2;
    // create a sphere that fits the volume size
    double smallestEdge = std::min(std::min(nx, ny), nz);
    double radius = 0.4 * smallestEdge;

    uchar grayscaleValueInside = 230;
    uchar grayscaleValueOutside = 35;

    VoxelVolume vol(nx, ny, nz);
    qCInfo(LC) << "Spherical Volume selected. Size: x =" << nx << " y =" << ny << " z ="<< nz;
    for (int x=0; x<nx; ++x) {
        for (int y=0; y<ny; ++y) {
            for (int z=0; z<nz; ++z) {
                // check if point (x,y,z) is inside or outside the sphere
                // with center (nx/2, ny/2, nz/2) and radius r=2*min(min(nx, ny), nz)/5
                // point (x, y, z) inside if current surface smaller than radius-surface
                if (std::pow((x - centerX), exponent)
                        + std::pow((y - centerY), exponent)
                        + std::pow((z - centerZ), exponent) <= std::pow(radius, exponent))
                    vol.setVoxel(x, y, z, grayscaleValueInside);
                else {
                    vol.setVoxel(x, y, z, grayscaleValueOutside);
                }
            }
        }
    }
    return vol;
}

// Demo-Volume: Cube
VoxelVolume createCubicDemoVolume(int nx, int ny, int nz)
{
    double centerX = nx/2;
    double centerY = ny/2;
    double centerZ = nz/2;
    double smallestEdge = std::min(std::min(nx, ny), nz);
    double halfLengthEdge = 0.3 * smallestEdge;

    uchar grayscaleValueInside = 230;
    uchar grayscaleValueOutside = 35;

    VoxelVolume vol(nx, ny, nz);
    qCInfo(LC) << "Cubic Volume selected. Size: x =" << nx << " y =" << ny << " z ="<< nz;
    for (int x=0; x<nx; ++x) {
        for (int y=0; y<ny; ++y) {
            for (int z=0; z<nz; ++z) {
                if (std::abs(z - centerZ) < halfLengthEdge
                        && std::abs(x - centerX) < halfLengthEdge
                        && std::abs(y - centerY) < halfLengthEdge)
                {
                    vol.setVoxel(x, y, z, grayscaleValueInside);
                } else {
                    vol.setVoxel(x, y, z, grayscaleValueOutside);
                }
            }
        }
    }
    return vol;
}

// Demo-Volume: Cube and Sphere
VoxelVolume createCubeAndSphereDemoVolume(int nx, int ny, int nz)
{
    double centerX = nx/2;
    double centerY = ny/2;
    double centerZ = nz/2;
    double quarterX = centerX/2; // center of cube
    double threeQuartersX = centerX*1.25; // origin of sphere
    double exponent = 2;

    double smallestEdge = std::min(std::min(nx, ny), nz);
    double radius = 0.15 * smallestEdge;
    double halfLengthEdge = radius;

    uchar grayscaleValueInside = 120;
    uchar grayscaleValueOutside = 0;

    VoxelVolume vol(nx, ny, nz);
    qCInfo(LC) << "Volume with cube and sphere selected. Size: x =" << nx << " y =" << ny << " z ="<< nz;
    for (int x=0; x<nx; ++x) {
        for (int y=0; y<ny; ++y) {
            for (int z=0; z<nz; ++z) {

                if (std::pow((x - threeQuartersX), exponent)
                        + std::pow((y - centerY), exponent)
                        + std::pow((z - centerZ), exponent)
                        <= std::pow(radius, exponent) ||
                        (std::abs(z - centerZ) < halfLengthEdge
                         && std::abs(x - quarterX) < halfLengthEdge
                         && std::abs(y - centerY) < halfLengthEdge))
                    vol.setVoxel(x, y, z, grayscaleValueInside);
                else {
                    vol.setVoxel(x, y, z, grayscaleValueOutside);
                }
            }
        }
    }
    return vol;
}

// Pentacam-Data Volume, fast
VoxelVolume createPentacamDataFastVolume(QString filename)
{
    VoxelVolume vol(128, 128, 128);

    QFile file(filename);

    QByteArrayList values;

    if(!file.open(QIODevice::ReadOnly))
        qDebug() << file.errorString();

    while(!file.atEnd()) // parse through file
    {
        for (int z=0; z<vol.depth(); ++z) {
            for (int y=0; y<vol.height(); ++y) {

                QByteArray readline = file.readLine(); // read a line

                values = readline.split(';'); // split into subarrays

                for (int x=0; x< vol.width(); ++x) {
                    bool ok = false;
                    int grayVal = values[x].toInt(&ok);
                    if (!ok) {
                        qWarning() << "conversion to int failed: " << values[x];
                        continue;
                    }
                    vol.setVoxel(z, y, x, static_cast<uchar>(grayVal));
                }
            }
        }
    }
    return vol;
}

// Pentacam-Data Volume, HQ
VoxelVolume createPentacamDataHQVolume(QString filename)
{
    VoxelVolume vol(256, 256, 256);

    QFile file(filename);

    QByteArrayList values;

    if(!file.open(QIODevice::ReadOnly))
        qDebug() << file.errorString();

    while(!file.atEnd()) // parse through file
    {
        for (int z=0; z<vol.depth(); ++z) {
            for (int y=0; y<vol.height(); ++y) {

                QByteArray readline = file.readLine(); // read a line

                values = readline.split(';'); // split into subarrays

                for (int x=0; x<vol.width(); ++x) {
                    bool ok = false;
                    int grayVal = values[x].toInt(&ok);
                    if (!ok) {
                        qWarning() << "conversion to int failed: " << values[x];
                        continue;
                    }
                    vol.setVoxel(z, y, x, static_cast<uchar>(grayVal));
                }
            }
        }
    }
    return vol;
}

} // namespace voxel
