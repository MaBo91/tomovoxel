#include "VoxelInFBORenderer.h"

#include <QLoggingCategory>

namespace voxel {

static Q_LOGGING_CATEGORY(LC, "de.oculus.penta.VoxelInFBORenderer", QtInfoMsg)

VoxelInFBORenderer::VoxelInFBORenderer(CameraControls *cameraControls, RenderControls *renderControls):
    m_voxelRenderer(cameraControls, renderControls)
{

}

// OpenGL rendering functions go here, called on every frame
void VoxelInFBORenderer::render()
{
    m_voxelRenderer.renderUnitCubeToTexture(m_voxelRenderer.FRONTFACES); // uses own FBO
    m_voxelRenderer.renderUnitCubeToTexture(m_voxelRenderer.BACKFACES); // uses own FBO
    m_mainFBO->bind();
    m_voxelRenderer.render();
    m_mainFBO->release();

    calculateFrameRate();
    update();
}

// specify our FramebufferObject, called up one time on init and if renderer size in QML window changes
QOpenGLFramebufferObject *VoxelInFBORenderer::createFramebufferObject(const QSize &size)
{
    QOpenGLFramebufferObjectFormat format;
    format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
    format.setSamples(4);
    m_mainFBO = new QOpenGLFramebufferObject(size, format);
    qCInfo(LC) << "New Framebuffer created. Size: " << size;
    m_voxelRenderer.init(size); // init-func of VoxelRenderer for code that doesn't have to be executed on every frame
    return m_mainFBO;
}

void VoxelInFBORenderer::calculateFrameRate()
{
    static float framesPerSecond = 0.0f;
    static float lastTime = 0.0f;

    float currentTime = GetTickCount() * 0.001f; // get current time
    ++framesPerSecond; // accumulate number of frames

    // if 1 second elapsed, print FPS to application output
    if(currentTime - lastTime > 1.0f)
    {
        lastTime = currentTime;
        qCInfo(LC) << "Current FPS" << static_cast<int>(framesPerSecond);
        framesPerSecond = 0;
    }
}

} // namespace voxel
