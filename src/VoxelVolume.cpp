#include "VoxelVolume.h"

#include <QLoggingCategory>
#include <QDebug>

namespace voxel {

static Q_LOGGING_CATEGORY(LC, "de.oculus.penta.VoxelVolume", QtInfoMsg);

VoxelVolume::VoxelVolume()
{

}

VoxelVolume::VoxelVolume(int width, int height, int depth)
    : m_width(width), m_height(height), m_depth(depth)
{
    m_values.resize(voxelVolumeSize());
    qCInfo(LC) << "Size of Byte Array:" << m_width <<
                "*" << m_height << "*" << m_depth <<
                " = " << m_values.size() << "Byte.";
}

int VoxelVolume::voxelVolumeSize() const
{
    return m_width * m_height * m_depth;
}

int VoxelVolume::width() const
{
    return m_width;
}

int VoxelVolume::height() const
{
    return m_height;
}

int VoxelVolume::depth() const
{
    return m_depth;
}

// return pointer to data
const char *VoxelVolume::data() const
{
   return m_values.constData();
}

int VoxelVolume::byteCount() const
{
    return m_values.size();
}

// calculate offset position in byte array out of coordinates (x, y, z)
int VoxelVolume::offset(int x, int y, int z) const
{
    return x + (y * m_width) + (z * m_width * m_height);
}

void VoxelVolume::setVoxel(int x, int y, int z, uchar value)
{
    if (x <= m_width && y <= m_height && z <= m_depth) {
        m_values[offset(x, y, z)] = static_cast<char>(value);
    } else {
        qFatal("Voxel out of voxelvolume range!");
    }
}

uchar VoxelVolume::voxel(int x, int y, int z) const
{
    if (x <= m_width && y <= m_height && z <= m_depth) {
        return static_cast<uchar>(m_values.at(offset(x,y,z)));
    } else {
        qFatal("Voxel out of voxelvolume range!");
    }
}

} // namespace voxel
