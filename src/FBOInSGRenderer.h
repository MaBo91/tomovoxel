#pragma once

#include "TomoVoxel_Export.h"

#include <QtQuick/QQuickFramebufferObject>

namespace voxel {

class CameraControls;
class RenderControls;


class TOMOVOXEL_DLL FBOInSGRenderer : public QQuickFramebufferObject
{
    Q_OBJECT
    Q_PROPERTY(QObject* cameraControls READ cameraControls NOTIFY cameraControlsChanged)
    Q_PROPERTY(QObject* renderControls READ renderControls NOTIFY renderControlsChanged)

public:
    FBOInSGRenderer();

    Renderer *createRenderer() const; // creates instance of our renderer
    QObject* cameraControls() const;
    QObject* renderControls() const;

signals:
    void cameraControlsChanged();
    void renderControlsChanged();

private:
    CameraControls* m_pCameraControls = nullptr;
    RenderControls* m_pRenderControls = nullptr;
};

} // namespace voxel
