#pragma once

#include "VoxelVolume.h"
#include <QString>

namespace voxel {

TOMOVOXEL_DLL VoxelVolume createSphericalDemoVolume(
        int nx=80,
        int ny=70,
        int nz=60
        );

TOMOVOXEL_DLL VoxelVolume createCubicDemoVolume(
        int nx=80,
        int ny=70,
        int nz=60
        );

TOMOVOXEL_DLL VoxelVolume createCubeAndSphereDemoVolume(
        int nx=80,
        int ny=70,
        int nz=60
        );

TOMOVOXEL_DLL VoxelVolume createPentacamDataFastVolume(QString filename);

TOMOVOXEL_DLL VoxelVolume createPentacamDataHQVolume(QString filename);
} // namespace voxel
