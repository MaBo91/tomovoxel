#include "helloShader.h"

#include <QMatrix4x4>

helloShader::helloShader()
{

}

void helloShader::init()
{
    m_glFuncs = QOpenGLContext::currentContext()->functions();

    // Create Shader
    m_program.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/VertexShaderTest.vert");
    m_program.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/FragmentShaderTest.frag");

    m_program.link();

    // define attribute/uniform location from c++ to glsl
    vertexLocation = m_program.attributeLocation("vertex");
    matrixLocation = m_program.uniformLocation("MVP");
}

void helloShader::render()
{
    // Rendering-Matritzen
    QMatrix4x4 model; // Model-to-World
    QMatrix4x4 view; // World-to-Camera
    QMatrix4x4 projection; // Camera-to-Perspective

    model.setToIdentity(); // Einheitsmatrix

    view.lookAt(
                QVector3D(0.0, 0.0, 5.0),  // Camera is at (0, 0, 5) in World-Space
                QVector3D(0.0, 0.0, 0.0),   // and looks at the origin
                QVector3D(0.0, 1.0, 0.0));  // Up-Vector

    projection.perspective(45.0, 800.0 / 600.0, 1.0, 100.0); // 45° Field of View, 800/600 Ratio, display range: 1 unit <-> 100 units

    QMatrix4x4 mvp = (projection * view * model); // ModelViewProjection-Matrix

    m_program.bind();
    m_program.setUniformValue(matrixLocation, mvp); // set value at location
    draw();
    m_program.release();
}

void helloShader::draw()
{
    // Vertices-Data
    static GLfloat const triangleVertices[] = {
        -1.0f, -1.0f, 0.0f,
        1.0f, -1.0f, 0.0f,
        0.0f, 1.0f, 0.0f
    };

    m_program.enableAttributeArray(vertexLocation);
    m_program.setAttributeArray(vertexLocation, triangleVertices, 3); // set value at location
    m_glFuncs->glDrawArrays(GL_TRIANGLES, 0, 3);
    m_program.disableAttributeArray(vertexLocation);
}



