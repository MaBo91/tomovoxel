#pragma once

#include "TomoVoxel_Export.h"
#include <QObject>

namespace voxel {

class TOMOVOXEL_DLL RenderControls : public QObject
{
    Q_OBJECT
    Q_PROPERTY(float overexposure READ overexposure WRITE setOverexposure NOTIFY overexposureChanged)
    Q_PROPERTY(float sampleStepSize READ sampleStepSize WRITE setSampleStepSize NOTIFY sampleStepSizeChanged)
    Q_PROPERTY(float materialThreshold READ materialThreshold WRITE setMaterialThreshold NOTIFY materialThresholdChanged)
    Q_PROPERTY(float alpha READ alpha WRITE setAlpha NOTIFY alphaChanged)
    Q_PROPERTY(float gamma READ gamma WRITE setGamma NOTIFY gammaChanged)
    Q_PROPERTY(float jitterFactor READ jitterFactor WRITE setJitterFactor NOTIFY jitterFactorChanged)

public:
    RenderControls();

    float sampleStepSize() const;
    void setSampleStepSize(float sampleStepSize);

    float overexposure() const;
    void setOverexposure(float overexposure);

    float materialThreshold() const;
    void setMaterialThreshold(float materialThreshold);

    float alpha() const;
    void setAlpha(float alpha);

    float gamma() const;
    void setGamma(float gamma);

    float jitterFactor() const;
    void setJitterFactor(float factor);

    bool haveMaterial() const;
    Q_INVOKABLE bool toggleHaveMaterial();

    bool maxIntensityProjection() const;
    Q_INVOKABLE bool toggleMaxIntensityProjection();

    bool alphaBlending() const;
    Q_INVOKABLE bool toggleAlphaBlending();

    bool volumeSelect() const;
    Q_INVOKABLE bool toggleVolumeSelect();

    bool jittering() const;
    Q_INVOKABLE bool toggleJittering();

    Q_INVOKABLE void increaseSampleSteps();
    Q_INVOKABLE void decreaseSampleSteps();

    Q_INVOKABLE void increaseOverexposure();
    Q_INVOKABLE void decreaseOverexposure();

    Q_INVOKABLE void reset();

signals:
    void overexposureChanged();
    void sampleStepSizeChanged();
    void materialThresholdChanged();
    void alphaChanged();
    void gammaChanged();
    void jitterFactorChanged();

private:
    float m_sampleStepSize;
    float m_overexposure; // 1.0: no overexposure, higher values increase overexposure
    bool m_haveMaterial;
    bool m_maxIntensityProjection;
    bool m_alphaBlending;
    bool m_volumeSelect;
    bool m_jittering;
    float m_materialThreshold;
    float m_alpha, m_gamma;
    float m_jitterFactor;
};

} // namespace voxel
