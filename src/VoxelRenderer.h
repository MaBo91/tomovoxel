#pragma once

#include "TomoVoxel_Export.h"

#include "VoxelVolume.h"
#include "CameraControls.h"
#include "RenderControls.h"

#include <QOpenGLFunctions_3_1>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>

namespace voxel {

class TOMOVOXEL_DLL VoxelRenderer
{
public:
    VoxelRenderer(CameraControls* cameraControls, RenderControls* renderControls);

    enum UnitCubeFaces {FRONTFACES, BACKFACES};

    void init(const QSize& size); // init function for code that will not be executed on every frame
    void render(); // render function for raycasting shader program

    QOpenGLTexture *voxelDataTo3DTexture(const VoxelVolume& volume); // transform voxel data from byte array to 3D texture

    void setImageSize(const QSize& size); // set correct renderer size
    void initUnitCubeTextures(); // init textures of bounding box
    void renderUnitCube(UnitCubeFaces faces); // function to render unit cube bounding box
    void renderUnitCubeToTexture(UnitCubeFaces faces); // render selected faces of unit cube, store in 2D texture
    void createJitterTexture(int size);

private:
    QOpenGLFunctions_3_1 *m_glFuncs;

    CameraControls* m_cameraControls;
    RenderControls* m_renderControls;

    QOpenGLShaderProgram m_programUnitCubeRendering;
    QOpenGLShaderProgram m_mainShaderProgram;

    QOpenGLTexture* m_voxelDataGrayValueTexture3D;
    QOpenGLTexture* m_voxelDataAlphaTexture3D;
    QOpenGLTexture* m_backfaceTexture2D;
    QOpenGLTexture* m_frontfaceTexture2D;
    QOpenGLTexture* m_jitterTexture2D;

    int m_imageWidth;
    int m_imageHeight;

    int m_jitterTextureSize = 32; // creates 32*32 jitter texture

    // handles for unit cube rendering & raycasting
    int m_vertexLocationID;
    int m_matrixLocationID;
    int m_UVsID;

    bool m_isInitialized = false;
};

} // namespace voxel
