import QtQuick 2.12
import QtQuick.Controls 2.5
import Rendering 1.0

ApplicationWindow {
    id: window
    visible: true
    width: 1000
    height: 1020

    Rectangle {
        color: "lightgray"
        anchors.fill: parent

        Column {
            id: main
            width: window.width
            height: window.height
            spacing: 20

            Column {
                id: rendererDisplay
                width: main.width * 0.8 * (window.width/window.height)
                height: rendererDisplay.width
                topPadding: 10
                anchors.horizontalCenter: main.horizontalCenter

                MouseArea {
                    id: mouseArea
                    width: rendererDisplay.width
                    height: rendererDisplay.height

                    property real lastXPos: 0
                    property real lastYPos: 0
                    property real offsetX: 0
                    property real offsetY: 0

                    acceptedButtons: Qt.AllButtons
                    onWheel: if(wheel.angleDelta.y > 0) fboInSgRenderer.cameraControls.zoomIn()
                             else fboInSgRenderer.cameraControls.zoomOut()

                    onEntered: {
                        lastXPos = mouseX;
                        lastYPos = mouseY;
                    }

                    onPositionChanged: {
                        offsetX = (lastXPos - mouseX) * 0.5;
                        offsetY = (lastYPos - mouseY) * 0.5;

                        fboInSgRenderer.cameraControls.updateMVP();

                        lastXPos = mouseX;
                        lastYPos = mouseY;
                    }

                    Binding {
                        target: fboInSgRenderer.cameraControls
                        property: "offsetX"
                        value: mouseArea.offsetX * (-1)
                    }

                    Binding {
                        target: fboInSgRenderer.cameraControls
                        property: "offsetY"
                        value: mouseArea.offsetY * (-1)
                    }

                    // our custom QML-Item
                    Renderer {
                        id: fboInSgRenderer
                        width: rendererDisplay.width
                        height: rendererDisplay.height
                    }

                    Row {
                        x: spacing / 2
                        y: rendererDisplay.height - spacing * 3
                        spacing: 10

                        Text {
                            id: textMaxSamples
                            text: qsTr("Max. Samples:")
                            color: "white"
                            font.pixelSize: 20
                        }

                        Text {
                            id: valueMaxSamples
                            text: (Math.sqrt(3) / sliderSampleStepSize.value).toFixed(0)
                            color: "white"
                            font.pixelSize: 20
                        }
                    }
                }
            }

            Row {
                id: userInputSpace
                width: main.width * 0.98
                height: main.height * 0.25
                anchors.horizontalCenter: main.horizontalCenter

                Row {
                    id: leftInputSpace
                    width: userInputSpace.width / 2
                    height: userInputSpace.height
                    spacing: 10

                    Column {
                        id: buttonTextColumn
                        height: leftInputSpace.height
                        spacing: leftButtonColumn.height / 6.5

                        Text {
                            id: textRotate
                            text: qsTr("Rotate")
                            font.pixelSize: 14
                        }

                        Text {
                            id: textYaw
                            text: qsTr("Yaw")
                            font.pixelSize: 14
                        }

                        Text {
                            id: textPitch
                            text: qsTr("Pitch")
                            font.pixelSize: 14
                        }

                        Text {
                            id: textRoll
                            text: qsTr("Roll")
                            font.pixelSize: 14
                        }
                    }

                    Column {
                        id: leftButtonColumn
                        spacing: 5

                        Button {
                            id: buttonRotateLeft
                            width: 70
                            height: 30
                            text: qsTr("<<")
                            onClicked: fboInSgRenderer.cameraControls.rotateLeft()
                        }

                        Button {
                            id: buttonIncreaseYaw
                            width: 70
                            height: 30
                            text: qsTr("+")
                            onClicked: fboInSgRenderer.cameraControls.increaseYaw()
                        }

                        Button {
                            id: buttonIncreasePitch
                            width: 70
                            height: 30
                            text: qsTr("+")
                            onClicked: fboInSgRenderer.cameraControls.increasePitch()
                        }

                        Button {
                            id: buttonIncreaseRoll
                            width: 70
                            height: 30
                            text: qsTr("+")
                            onClicked: fboInSgRenderer.cameraControls.increaseRoll()
                        }

                    }

                    Column {
                        id: rightButtonColumn
                        spacing: 5

                        Button {
                            id: buttonRotateRight
                            width: 70
                            height: 30
                            text: qsTr(">>")
                            onClicked: fboInSgRenderer.cameraControls.rotateRight()
                        }

                        Button {
                            id: buttonDecreaseYaw
                            width: 70
                            height: 30
                            text: qsTr("-")
                            onClicked: fboInSgRenderer.cameraControls.decreaseYaw()
                        }

                        Button {
                            id: buttonDecreasePitch
                            width: 70
                            height: 30
                            text: qsTr("-")
                            onClicked: fboInSgRenderer.cameraControls.decreasePitch()
                        }

                        Button {
                            id: buttonDecreaseRoll
                            width: 70
                            height: 30
                            text: qsTr("-")
                            onClicked: fboInSgRenderer.cameraControls.decreaseRoll()
                        }
                    }

                    Column {
                        id: resetButtonCheckBoxesColumn
                        spacing: 5

                        Row {
                            spacing: 10

                            Button {
                                id: buttonResetView
                                width: 70
                                height: 30
                                text: "Reset"
                                font.pointSize: 9
                                onClicked: { fboInSgRenderer.cameraControls.resetView();
                                    fboInSgRenderer.renderControls.reset();
                                    sliderOverexposure.value = 1.0;
                                    sliderSampleStepSize.value = 0.01;
                                    sliderMaterialThreshold.value = 0.5;
                                    sliderGamma.value = 1.0;
                                    checkBoxHaveMaterial.checked = false;
                                    checkBoxMaxIntensityProjection.checked = false;
                                    checkBoxAlphaBlending.checked = false;
                                }
                            }

                            Button {
                                id: buttonChangeVolume
                                width: 170
                                height: 30
                                text: "View Alpha Volume"
                                font.pointSize: 9
                                onClicked: {
                                    fboInSgRenderer.renderControls.toggleVolumeSelect();

                                    if(buttonChangeVolume.text == "View Alpha Volume")
                                        buttonChangeVolume.text = "View Grayvalue Volume";
                                    else
                                        buttonChangeVolume.text = "View Alpha Volume";
                                }
                            }
                        }

                        CheckBox {
                            id: checkBoxMaxIntensityProjection
                            text: qsTr("Max. Intensity Projection")
                            onCheckedChanged: fboInSgRenderer.renderControls.toggleMaxIntensityProjection();
                        }

                        CheckBox {
                            id: checkBoxHaveMaterial
                            text: qsTr("Show Material")
                            onCheckedChanged: fboInSgRenderer.renderControls.toggleHaveMaterial();
                        }

                        CheckBox {
                            id: checkBoxAlphaBlending
                            text: qsTr("Alpha Blending")
                            onCheckedChanged: fboInSgRenderer.renderControls.toggleAlphaBlending();
                        }

                        CheckBox {
                            id: checkBoxJittering
                            text: qsTr("Jittering");
                            onCheckedChanged: fboInSgRenderer.renderControls.toggleJittering();
                        }
                    }
                }

                Row {
                    id: rightInputSpace
                    width: userInputSpace.width / 2
                    height: userInputSpace.height
                    spacing: 10

                    Column {
                        id: sliderTextColumn
                        spacing: rightInputSpace.height / 10

                        Text {
                            id: sampleStepSizeText
                            text: qsTr("Sample Stepsize")
                            font.pixelSize: 14
                        }

                        Text {
                            id: overexposureText
                            text: if(!checkBoxJittering.checked)
                                      qsTr("Overexposure")
                                  else qsTr("Jitter Factor");
                            font.pixelSize: 14
                        }

                        Text {
                            id: materialThresholdText
                            text: qsTr("Material Threshold")
                            font.pixelSize: 14
                        }

                        Text {
                            id: gammaText
                            text: qsTr("Gamma")
                            font.pixelSize: 14
                        }

                        Text {
                            id: alphaText
                            text: qsTr("Alpha")
                            font.pixelSize: 14
                        }
                    }

                    Column {
                        id: sliderColumn

                        Binding {
                            target: fboInSgRenderer.renderControls
                            property: "sampleStepSize"
                            value: sliderSampleStepSize.value
                        }

                        Slider {
                            id: sliderSampleStepSize
                            stepSize: 0.0000001
                            from: 0.001
                            value: 0.01
                            to: 0.05
                        }

                        Binding {
                            target: fboInSgRenderer.renderControls
                            property: if(!checkBoxJittering.checked)
                                          "overexposure";
                                      else "jitterFactor";
                            value: sliderOverexposure.value
                        }

                        Slider {
                            id: sliderOverexposure
                            stepSize: 0.01
                            from: 0
                            value: 1.0
                            to: 30
                        }

                        Binding {
                            target: fboInSgRenderer.renderControls
                            property: "materialThreshold"
                            value: sliderMaterialThreshold.value
                        }

                        Slider {
                            id: sliderMaterialThreshold
                            stepSize: 0.000001
                            from: 0.0
                            value: 0.5
                            to: 1.0
                        }

                        Binding {
                            target: fboInSgRenderer.renderControls
                            property: "gamma"
                            value: sliderGamma.value
                        }

                        Slider {
                            id: sliderGamma
                            stepSize: 0.001
                            from: 0.0
                            value: 1.0
                            to: 25.0
                        }

                        Binding {
                            target: fboInSgRenderer.renderControls
                            property: "alpha"
                            value: sliderAlpha.value
                        }

                        Slider {
                            id: sliderAlpha
                            stepSize: 0.001
                            from: 0.0
                            value: 1.0
                            to: 1.0
                        }
                    }

                    Column {
                        id: sliderValueColumn
                        spacing: rightInputSpace.height / 10

                        Text {
                            id: sampleStepSizeValue
                            text: sliderSampleStepSize.value.toFixed(5)
                            font.pixelSize: 14
                        }

                        Text {
                            id: overExposureValue
                            text: sliderOverexposure.value.toFixed(2)
                            font.pixelSize: 14
                        }

                        Text {
                            id: materialThresholdValue
                            text: sliderMaterialThreshold.value.toFixed(2)
                            font.pixelSize: 14
                        }

                        Text {
                            id: gammaValue
                            text: sliderGamma.value.toFixed(2)
                            font.pixelSize: 14
                        }

                        Text {
                            id: alphaValue
                            text: sliderAlpha.value.toFixed(2)
                            font.pixelSize: 14
                        }
                    }
                }
            }
        }
    }
}

